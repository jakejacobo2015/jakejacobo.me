<?php
  $Full_Name =  "Jake Jacobo";
  $Age="24 y.o";
  $Work_Title="Software Developer";
  $Head_Details="Seeking a position of Software Developer to put 4 years of learnings into use to help the business meet strategic and operational goals by identifying opportunities to deploy new technology. Possess expertise in networking and hardware, superior technical aptitude, and proven ability to manage complex tasks.";
  $Phone_Number="09984193301";
  $Telephone_Number="(047) 222 - 0616";
  $Email="jakeamabajacobo@gmail.com";
  $WorkEmail = "jjacobo@softechgroupinc.com";
  $Address="Blk 23 Ubas St Water Dam";
  $District = "G.H";
  $City ="Olongapo City";
  $State ="Zambales";
  $ZipCode = "2200";


  $WorkExprienceTitle1="Software Developer";
  $WorkExprienceDate1="OCT 2016 – Present";
  $WorkExprienceCompany1="Linneman Technologies Inc";
  $WorkExprienceDescription1="Maintain and development Product Life cycle Management(PLM) Software, Softech ProductCenter . Lead and train software developer team for projects and teask management. Use different technologies: MS Visual studio , Windows MFC, Oracle, MySQL Server, IBM Clearcase and different programming language & Tags: C/C++ , WML , XML , HTML5, CSS , JAVASCRIPT,JQUERY and Web Extension Applications.";

  $WorkExprienceTitle2="Part-time Assistant Lecture 2";
  $WorkExprienceDate2="NOV 2018 - Present";
  $WorkExprienceCompany2="Lyceum Subic Bay";
  $WorkExprienceDescription2="Teaching I.T subjects like Computer Programming, CISCO Network and Computer Hardware Systems.";

  $WorkExprienceTitle3="Mobile Application Developer";
  $WorkExprienceCompany3="Perpetual Wave Inc.";
  $WorkExprienceDate3="MAR 2015 - SEP 2016";
  $WorkExprienceDescription3="Design and build application for Android platform
  Agile & Scrum Development 
  Develop for Mobile Cross Platform Application to support Android and IOS using React-Native JS 
  Design Front-End XML Android UI for support Different Screen sizes 
  Work on bug fix ,crash issue and improving the android application performance
  Data Source , JSON and Android/WEB API's.
  ";

  $WorkExprienceTitle4="Software Developer";
  $WorkExprienceCompany4="";
  $WorkExprienceDate4="";
  $WorkExprienceDescription4="";




  $EducationCoure1 = "Bachelor of Science in Information Technology";
  $EducationSchool1 = "AMA Computer College";
  $EducationAddress1 = "AYC Bldg. 1670 Rizal Ave. East Bajac-Bajac Olongapo City  Philippines";
  $EducationDate1 = "2011 - 2015";



  $EducationCoure2 = "Computer System Servicing";
  $EducationSchool2 = "Professional Institute of Modern Education (PRIME) Inc.";
  $EducationAddress2 = "#1939 Rizal Avenue West, Bajac-Bajac, Olongapo City";
  $EducationDate2 = "2018";



  $CertificationName2 = "CCNA1: Network Fundamentals";
  $CertificationDate2 = "NOV 19 2013";
  $CertificatonDetails2 = "CISCO Networking Academy  AMA Computer College
  Network Basic, 7 OSI Models , Types ,IP Addressing 
  ";


  $CertificationName3 = "CCNA2: Routing Protocols and Concepts";
  $CertificationDate3 = "APR 14 2014";
  $CertificatonDetails3 = "CISCO Networking  Academy AMA Computer College
  Configure Routers ,Dif. Concepts,Switches,RIPV2,EIGRP, WAN Tech.
  ";


  $CertificationName1 = "Computer System Servicing NC II TESDA";
  $CertificationDate1 = "OCT 28 2018";
  $CertificatonDetails1 = "Professional Institute of Modern Education (PRIME) Inc. Computer Hardware, Software and Network maintenance and troubleshooting. ";


  $ProfessionalSkils1 = "HTML";
  $ProfessionalPercentage1 = "95";


  $ProfessionalSkils2 = "HTML";
  $ProfessionalPercentage2 = "95";

  ?>

