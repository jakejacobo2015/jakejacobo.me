<?php require_once 'jjacobo.php';?>
<!DOCTYPE html>

<html lang="en">
<head>
        <meta charset="utf-8">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140670496-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-140670496-1');
        </script>


        <!-- TITLE OF SITE-->
        <title><?php echo $Full_Name; ?></title>
        
        <!-- META TAG -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="CV, Portfolio, Resume">
        <meta name="author" content="<?php echo $Full_Name; ?>">
        
        <!-- FAVICON -->
        <link rel="icon" href="assets/images/JLOGO.png">
        <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.html">
        <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png">

        <!-- ========================================
                Stylesheets
        ==========================================--> 
        
        <!-- MATERIALIZE CORE CSS -->
        <link href="assets/css/materialize.min.css" rel="stylesheet">
        

        <!-- ADDITIONAL CSS -->
        <link rel="stylesheet" href="assets/css/animate.css">
        

        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,400italic,700italic' rel='stylesheet' type='text/css'>
        

        <!--FONTAWESOME CSS-->
        <link href="assets/icons/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 
        

        <!-- CUSTOM STYLE -->
        <link href="assets/css/style.css" rel="stylesheet">
        

        <!-- RESPONSIVE CSS-->
        <link href="assets/css/responsive.css" rel="stylesheet">

        <!-- COLORS -->        
        <link rel="alternate stylesheet" href="assets/css/colors/red.css" title="red">
        <link rel="alternate stylesheet" href="assets/css/colors/purple.css" title="purple">
        <link rel="alternate stylesheet" href="assets/css/colors/orange.css" title="orange">
        <link rel="alternate stylesheet" href="assets/css/colors/green.css" title="green">
        <link rel="stylesheet" href="assets/css/colors/lime.css" title="lime">
        <link rel="stylesheet" href="./assets/css/custom.css">
        

        
    </head>
    <body>
        <!-- Start Container-->
        <div class="container">
            <!-- row -->
            <div class="row">
            <!-- =========================================
                           SIDEBAR   
            ==========================================-->
                <!-- Start Sidebar -->
                <aside class="col l4 m12 s12 sidebar z-depth-1" id="sidebar">
                    <!--  Sidebar row -->
                    <div class="row">                      
                        <!--  top section   -->
                        <div class="heading">                            
                            <!-- ====================
                                      IMAGE   
                            ==========================-->
                            <div class="feature-img">
                                <a href="./"><img src="assets/images/jakejacobo2x2.png" class="responsive-img" alt=""></a> 
                            </div>                            
                            <!-- =========================================
                                       NAVIGATION   
                            ==========================================-->
                            <div class=" nav-icon">
                                <nav>
                                    <div class="nav-wrapper">
                                      <ul id="nav-mobile" class="side-nav">                                  
                                        <li><a href="https://drive.google.com/file/d/1PWL0HU-dlyVWRpVqsPcmuj_AGWyfJiCX/view?usp=sharing"  target="_blank" >Resume</a></li>                                        
                                        <li><a href="./project/">Projects</a></li>
                                        <li><a href="./contact/">Contact</a></li>
                                      </ul>
                                      <a href="#" data-activates="nav-mobile" class="button-collapse  "><i class="mdi-navigation-menu"></i></a>
                                    </div>
                                </nav>
                            </div>                            
                            <!-- ========================================
                                       NAME AND TAGLINE
                            ==========================================--><br> 
                            <div class="title col s12 m12 l9 right  wow fadeIn" data-wow-delay="0.1s">
                                <h3 class="full_name"  style="font-size:37px !important;"><?php echo $Full_Name;?></h3> <!-- title name -->
                                <span class="work_title" style="font-size:15px;"><?php echo $Work_Title;  ?></span>  <!-- tagline -->
                            </div>                         
                        </div>
                         <!-- sidebar info -->
                        <div class="col l12 m12 s12 sort-info sidebar-item">
                            <div class="row">                               
                                <div class="col m12 s12 l3 icon"> <!-- icon -->
                                   <i class="fa fa-user"></i>
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a1" data-wow-delay="0.1s" > <!-- text -->
                                    <div class="section-item-details">
                                        <p><?php echo $Head_Details; ?></p>
                                    </div>             
                                </div>
                            </div>         
                        </div>
                        <!-- MOBILE NUMBER -->
                        <div class="col l12 m12 s12  mobile sidebar-item">
                            <div class="row">                                
                                <div class="col m12 s12 l3 icon">
                                    <i class="fa fa-phone"></i> <!-- icon -->
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a2" data-wow-delay="0.2s" >
                                    <div class="section-item-details">
                                        <div class="personal">
                                            <h4><a href="tel:<?php echo $Phone_Number; ?>"><?php echo $Phone_Number; ?></a></h4> <!-- Number -->             
                                            <span>mobile</span> 
                                        </div>
                                        <div class="work">
                                            <h4><a href="tel:<?php echo $Telephone_Number; ?>"><?php echo $Telephone_Number; ?></a></h4> <!-- Number -->
                                            <span>work</span> 
                                        </div>
                                    </div>
                                </div>
                            </div>             
                        </div>
                        <!--  EMAIL -->
                        <div class="col l12 m12 s12  email sidebar-item ">
                            <div class="row">                                
                                <div class="col m12 s12 l3 icon">
                                    <i class="fa fa-envelope"></i> <!-- icon -->
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a3" data-wow-delay="0.3s">
                                    <div class="section-item-details">
                                        <div class="personal">                                    
                                            <h4><a href="mailto:<?php echo $Email; ?>"><?php echo $Email; ?></a></h4> <!-- Email -->
                                            <span>personal</span> 
                                        </div>
                                        <div class="work">                                 
                                            <h4><a href="mailto:<?php echo $WorkEmail; ?>"><?php echo $WorkEmail; ?></a></h4> <!-- Email -->
                                            <span>work</span> <br>
                                        </div>
                                    </div>
                                </div> 
                            </div>          
                        </div>
                        <!-- ADDRESS  -->
                        <div class="col l12 m12 s12  address sidebar-item ">
                            <div class="row">                                
                                <div class="col l3 m12  s12 icon">
                                    <i class="fa fa-home"></i> <!-- icon -->
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a4" data-wow-delay="0.4s">
                                    <div class="section-item-details">
                                        <div class="address-details"> <!-- address  -->
                                            <h4><?php echo $Address; ?></span><br>
                                            <?php echo $District; ?>,  <?php echo $City; ?>.<br>
                                            <span><?php echo $State; ?>  <?php echo $ZipCode; ?></span></h4> 
                                        </div>                         
                                    </div>
                                </div>
                            </div>            
                        </div>

                        <!-- SKILLS -->
                        <div class="col l12 m12 s12  skills sidebar-item" >
                            <div class="row">
                                <div class="col m12 l3 s12 icon">
                                    <i class="fa fa-bar-chart-o"></i> <!-- icon -->
                                </div>
                                 <!-- Skills -->
                                <div class="col m12 l9 s12 skill-line a5 wow fadeIn" data-wow-delay="0.5s">
                                    <h3>Professional Skills </h3>
                                    
                            
                                    <span>HTML</span>
                                    <div class="progress">
                                        <div class="determinate"> 95%</div>
                                    </div>
                                    
                                    <span>CSS</span>
                                    <div class="progress">
                                        <div class="determinate">90%</div>
                                    </div>

                                    <span>Javascript</span>
                                    <div class="progress">
                                        <div class="determinate">85%</div>
                                    </div>

                                    <span>PHP</span>
                                    <div class="progress">
                                        <div class="determinate">70%</div>
                                    </div>

                                    <span>JAVA</span>
                                    <div class="progress">
                                        <div class="determinate">75%</div>
                                    </div>

                                     <span>XML</span>
                                    <div class="progress">
                                        <div class="determinate">45%</div>
                                    </div>

                                    <span>MYSQL</span>
                                    <div class="progress">
                                        <div class="determinate">40%</div>
                                    </div>

                                    <span>MONGODB</span>
                                    <div class="progress">
                                        <div class="determinate">40%</div>
                                    </div>


                                    <span>Node.js</span>                                    
                                    <div class="progress">
                                        <div class="determinate"> 70% </div>
                                    </div>
                                    
                                    <span>React-Native.js</span>                                    
                                    <div class="progress">
                                        <div class="determinate"> 40% </div>
                                    </div>

                                    <span>RubyOnRails</span>                                    
                                    <div class="progress">
                                        <div class="determinate"> 50% </div>
                                    </div>

                                     <span>C++</span>                                    
                                    <div class="progress">
                                        <div class="determinate"> 80% </div>
                                    </div>
                                    

                                </div>
                            </div>
                        </div>




                    </div>   <!-- end row -->
                </aside><!-- end sidebar -->

                <!-- =========================================
                   Work experiences
                ==========================================-->

                <section class="col s12 m12 l8 section">
                    <div class="row">
                        <div class="section-wrapper z-depth-1">                            
                            <div class="section-icon col s12 m12 l2">
                                <i class="fa fa-suitcase"></i>
                            </div>
                            <div class="custom-content col s12 m12 l10 wow fadeIn a1" data-wow-delay="0.1s">
                                <h2>Work Experience</h2>

                                <div class="custom-content-wrapper wow fadeIn a2" data-wow-delay="0.2s">
                                    <h3><?php echo $WorkExprienceTitle1;?><span>@<?php echo $WorkExprienceCompany1;?></span></h3>
                                    <span><?php echo $WorkExprienceDate1;?> </span>
                                    <p><?php echo $WorkExprienceDescription1;?> </p>
                                </div>
                                <div class="custom-content-wrapper wow fadeIn a3" data-wow-delay="0.3s">
                                <h3><?php echo $WorkExprienceTitle2;?><span>@<?php echo $WorkExprienceCompany2;?></span></h3>
                                    <span><?php echo $WorkExprienceDate2;?> </span>
                                    <p><?php echo $WorkExprienceDescription2;?> </p>
                                </div>
                                <div class="custom-content-wrapper wow fadeIn a4" data-wow-delay="0.4s">
                                <h3><?php echo $WorkExprienceTitle3;?><span>@<?php echo $WorkExprienceCompany3;?></span></h3>
                                    <span><?php echo $WorkExprienceDate3;?> </span>
                                    <p><?php echo $WorkExprienceDescription3;?> </p>
                                </div>
                            </div>                            
                        </div>

                        <!-- ========================================
                         Education 
                        ==========================================-->

                        <div class="section-wrapper z-depth-1">
                            <div class="section-icon col s12 m12 l2">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <div class="custom-content col s12 m12 l10 wow fadeIn a1" data-wow-delay="0.1s" >
                                <h2>Education </h2>
                                
                                <div class="custom-content-wrapper wow fadeIn a2" data-wow-delay="0.2s" >
                                    <h3><?php echo $EducationCoure1;?><span>@<?php echo $EducationSchool1;?></span></h3>
                                    <span><?php echo $EducationDate1;?></span>
                                    <p><?php echo $EducationAddress1;?></p>
                                </div>
                      

                                <div class="custom-content-wrapper wow fadeIn a2" data-wow-delay="0.2s" >
                                    <h3><?php echo $EducationCoure2;?><span>@<?php echo $EducationSchool2;?></span></h3>
                                    <span><?php echo $EducationDate2;?></span>
                                    <p><?php echo $EducationAddress2;?></p>
                                </div>
                      
                             
                            </div>
                        </div>



                        <!-- ========================================
                         Certifications 
                        ==========================================-->

                        <div class="section-wrapper z-depth-1">
                            <div class="section-icon col s12 m12 l2">
                                <i class="fa fa-certificate"></i>
                            </div>
                            <div class="custom-content col s12 m12 l10 wow fadeIn a1" data-wow-delay="0.1s" >
                                <h2>Certification</h2>
                                
                                <div class="custom-content-wrapper wow fadeIn a2" data-wow-delay="0.2s">
                                    <h3><?php echo $CertificationName1;?></h3>
                                    <span><?php echo $CertificationDate1;?> </span>
                                    <p><?php echo $CertificatonDetails1;?> </p>
                                </div>
                      
                                <div class="custom-content-wrapper wow fadeIn a2" data-wow-delay="0.2s">
                                    <h3><?php echo $CertificationName2;?></h3>
                                    <span><?php echo $CertificationDate2;?> </span>
                                    <p><?php echo $CertificatonDetails2;?> </p>
                                </div>
                      
                                <div class="custom-content-wrapper wow fadeIn a2" data-wow-delay="0.2s">
                                    <h3><?php echo $CertificationName3;?></h3>
                                    <span><?php echo $CertificationDate3;?> </span>
                                    <p><?php echo $CertificatonDetails3;?> </p>
                                </div>
                      

                             
                            </div>
                        </div>




                        <!-- ========================================
                              Intertests 
                        ==========================================-->

                        <div class="section-wrapper z-depth-1">                           
                            <div class="section-icon col s12 m12 l2">
                                <i class="fa fa-plane"></i>
                            </div>
                            <div class="interests col s12 m12 l10 wow fadeIn" data-wow-delay="0.1s"> 
                                <h2>Interestes </h2>
                                <ul> <!-- interetsr icon start -->
                                    <li><i class="fa fa-book tooltipped" data-position="top" data-delay="50" data-tooltip="Study"></i></li>
                                    <li><i class="fa fa-gamepad tooltipped" data-position="top" data-delay="50" data-tooltip="Gaming"></i></li>
                                    <li><i class="fa fa-headphones tooltipped" data-position="top" data-delay="50" data-tooltip="Music"></i></li>
                                    <li><i class="fa  fa-car tooltipped" data-position="top" data-delay="50" data-tooltip="Driving"></i></li>
                                    <li><i class="fa fa-coffee tooltipped" data-position="top" data-delay="50" data-tooltip="Coffee"></i></li>
                                </ul> <!-- interetsr icon end -->
                            </div>                          
                        </div>




                        <!-- =======================================
                          portfolio Website
                        ==========================================-->

                        <div class="section-wrapper z-depth-1 wow fadeIn" data-wow-delay="0.1s">                            
                            <div class="col s12 m12 l10 website right" >
                                <div class="row">
                                    <div class="col s12 m12 l6">
                                       <span><a href="http://www.jakejacobo.me/"  target="_blank">www.jakejacobo.me</a></span>
                                    </div>
                                    <div class="col col s12 m12 l6">
                                        <span><a href="https://ph.linkedin.com/in/jake-jacobo-5415b4b9" target="_blank">https://ph.linkedin.com/in/jake-jacobo-5415b4b9</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>



                        
                    </div><!-- end row -->
                </section><!-- end section -->
            </div> <!-- end row -->
        </div>  <!-- end container -->
        
 <!--=====================
                JavaScript
        ===================== -->
        <!-- Jquery core js-->
        <script src="assets/js/jquery.min.js"></script>
        
        <!-- materialize js-->
        <script src="assets/js/materialize.min.js"></script>
        
        <!-- wow js-->
        <script src="assets/js/wow.min.js"></script>
        
        <!-- Map api -->
        <script src="http://maps.googleapis.com/maps/api/js?v=3.exp"></script>
        
        <!-- Masonry js-->
        <script src="assets/js/masonry.pkgd.js"></script>

        <script src="assets/js/validator.min.js"></script>
        
        <!-- Customized js -->
        <script src="assets/js/init.js"></script>
    
     

    </body>

</html>
