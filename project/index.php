<?php require_once '../jjacobo.php';?>
<!DOCTYPE html>
<!--[if IE 7]><html class="no-js ie7 oldie" lang="en-US"> <![endif]-->
<!--[if IE 8]><html class="no-js ie8 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">

<head>
        <meta charset="utf-8">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140670496-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-140670496-1');
        </script>

             
        <!-- TITLE OF SITE-->
        <title><?php echo $Full_Name; ?></title>
        
        <!-- META TAG -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="CV, Portfolio, Resume">
        <meta name="author" content="<?php echo $Full_Name; ?>">
        
        <!-- FAVICON -->
        <link rel="icon" href="../assets/images/JLOGO.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/apple-icon-76x76.html">
        <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/apple-icon-114x114.png">

        <!-- ========================================
                Stylesheets
        ==========================================--> 
        
        <!-- MATERIALIZE CORE CSS -->
        <link href="../assets/css/materialize.min.css" rel="stylesheet">
        

        <!-- ADDITIONAL CSS -->
        <link rel="stylesheet" href="../assets/css/animate.css">
        

        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,400italic,700italic' rel='stylesheet' type='text/css'>
        

        <!--FONTAWESOME CSS-->
        <link href="../assets/icons/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 
        

        <!-- CUSTOM STYLE -->
        <link href="../assets/css/nav.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">
        

        <!-- RESPONSIVE CSS-->
        <link href="../assets/css/responsive.css" rel="stylesheet">

        <!-- COLORS -->        
        <link rel="alternate stylesheet" href="../assets/css/colors/red.css" title="red">
        <link rel="alternate stylesheet" href="../assets/css/colors/purple.css" title="purple">
        <link rel="alternate stylesheet" href="../assets/css/colors/orange.css" title="orange">
        <link rel="alternate stylesheet" href="../assets/css/colors/green.css" title="green">
        <link rel="stylesheet" href="../assets/css/colors/lime.css" title="lime">
        <link rel="stylesheet" href="../assets/css/custom.css">
        

        
    </head>
    <body>
        <!-- Start Container-->
        <div class="container">
            <!-- row -->
            <div class="row">

     <!-- =========================================
                           SIDEBAR   
            ==========================================-->
                <!-- Start Sidebar -->
                <aside class="col l4 m12 s12 sidebar z-depth-1" id="sidebar">
                    <!--  Sidebar row -->
                    <div class="row">                      
                        <!--  top section   -->
                        <div class="heading">                            
                            <!-- ====================
                                      IMAGE   
                            ==========================-->
                            <div class="feature-img">
                                <a href="./"><img src="../assets/images/jakejacobo2x2.png" class="responsive-img" alt=""></a> 
                            </div>                            
                            <!-- =========================================
                                       NAVIGATION   
                            ==========================================-->
                            <div class=" nav-icon">
                                <nav>
                                    <div class="nav-wrapper">
                                      <ul id="nav-mobile" class="side-nav">                                  
                                        <li><a href="../">Home</a></li>
                                        <li><a href="https://drive.google.com/file/d/1PWL0HU-dlyVWRpVqsPcmuj_AGWyfJiCX/view?usp=sharing"  target="_blank" >Resume</a></li>                                           
                                        <li><a href="./">Projects</a></li>
                                        <li><a href="../contact">Contact</a></li>
                                      </ul>
                                      <a href="#" data-activates="nav-mobile" class="button-collapse  "><i class="mdi-navigation-menu"></i></a>
                                    </div>
                                </nav>
                            </div>                            
                            <!-- ========================================
                                       NAME AND TAGLINE
                            ==========================================--><br> 
                            <div class="title col s12 m12 l9 right  wow fadeIn" data-wow-delay="0.1s">
                                <h3 class="full_name"  style="font-size:37px !important;"><?php echo $Full_Name;?></h3> <!-- title name -->
                                <span class="work_title" style="font-size:15px;"><?php echo $Work_Title;  ?></span>  <!-- tagline -->
                            </div>                         
                        </div>
                         <!-- sidebar info -->
                        <div class="col l12 m12 s12 sort-info sidebar-item">
                            <div class="row">                               
                                <div class="col m12 s12 l3 icon"> <!-- icon -->
                                   <i class="fa fa-user"></i>
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a1" data-wow-delay="0.1s" > <!-- text -->
                                    <div class="section-item-details">
                                    <p><?php echo $Head_Details; ?></p>
                                    </div>             
                                </div>
                            </div>         
                        </div>
                        <!-- MOBILE NUMBER -->
                        <div class="col l12 m12 s12  mobile sidebar-item">
                            <div class="row">                                
                                <div class="col m12 s12 l3 icon">
                                    <i class="fa fa-phone"></i> <!-- icon -->
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a2" data-wow-delay="0.2s" >
                                    <div class="section-item-details">
                                        <div class="personal">
                                            <h4><a href="tel:<?php echo $Phone_Number; ?>"><?php echo $Phone_Number; ?></a></h4> <!-- Number -->             
                                            <span>mobile</span> 
                                        </div>
                                        <div class="work">
                                            <h4><a href="tel:<?php echo $Telephone_Number; ?>"><?php echo $Telephone_Number; ?></a></h4> <!-- Number -->
                                            <span>work</span> 
                                        </div>
                                    </div>
                                </div>
                            </div>             
                        </div>
                        <!--  EMAIL -->
                        <div class="col l12 m12 s12  email sidebar-item ">
                            <div class="row">                                
                                <div class="col m12 s12 l3 icon">
                                    <i class="fa fa-envelope"></i> <!-- icon -->
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a3" data-wow-delay="0.3s">
                                    <div class="section-item-details">
                                        <div class="personal">                                    
                                            <h4><a href="mailto:<?php echo $Email; ?>"><?php echo $Email; ?></a></h4> <!-- Email -->
                                            <span>personal</span> 
                                        </div>
                                        <div class="work">                                 
                                            <h4><a href="mailto:<?php echo $WorkEmail; ?>"><?php echo $WorkEmail; ?></a></h4> <!-- Email -->
                                            <span>work</span> <br>
                                        </div>
                                    </div>
                                </div> 
                            </div>          
                        </div>
                        <!-- ADDRESS  -->
                        <div class="col l12 m12 s12  address sidebar-item ">
                            <div class="row">                                
                                <div class="col l3 m12  s12 icon">
                                    <i class="fa fa-home"></i> <!-- icon -->
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a4" data-wow-delay="0.4s">
                                    <div class="section-item-details">
                                        <div class="address-details"> <!-- address  -->
                                            <h4><?php echo $Address; ?></span><br>
                                            <?php echo $District; ?>,  <?php echo $City; ?>.<br>
                                            <span><?php echo $State; ?>  <?php echo $ZipCode; ?></span></h4> 
                                        </div>                         
                                    </div>
                                </div>
                            </div>            
                        </div>
                        <!-- SKILLS -->
                        <div class="col l12 m12 s12  skills sidebar-item" >
                            <div class="row">
                                <div class="col m12 l3 s12 icon">
                                    <i class="fa fa-bar-chart-o"></i> <!-- icon -->
                                </div>
                                 <!-- Skills -->
                                <div class="col m12 l9 s12 skill-line a5 wow fadeIn" data-wow-delay="0.5s">
                                    <h3>Professional Skills </h3>
                                    
                                    <span>HTML</span>
                                    <div class="progress">
                                        <div class="determinate"> 95%</div>
                                    </div>
                                    
                                    <span>CSS</span>
                                    <div class="progress">
                                        <div class="determinate">90%</div>
                                    </div>

                                    <span>Javascript</span>
                                    <div class="progress">
                                        <div class="determinate">85%</div>
                                    </div>

                                    <span>PHP</span>
                                    <div class="progress">
                                        <div class="determinate">70%</div>
                                    </div>

                                    <span>JAVA</span>
                                    <div class="progress">
                                        <div class="determinate">75%</div>
                                    </div>

                                     <span>XML</span>
                                    <div class="progress">
                                        <div class="determinate">45%</div>
                                    </div>

                                    <span>MYSQL</span>
                                    <div class="progress">
                                        <div class="determinate">40%</div>
                                    </div>

                                    <span>MONGODB</span>
                                    <div class="progress">
                                        <div class="determinate">40%</div>
                                    </div>


                                    <span>Node.js</span>                                    
                                    <div class="progress">
                                        <div class="determinate"> 70% </div>
                                    </div>
                                    
                                    <span>React-Native.js</span>                                    
                                    <div class="progress">
                                        <div class="determinate"> 40% </div>
                                    </div>

                                    <span>RubyOnRails</span>                                    
                                    <div class="progress">
                                        <div class="determinate"> 50% </div>
                                    </div>

                                     <span>C++</span>                                    
                                    <div class="progress">
                                        <div class="determinate"> 80% </div>
                                    </div>

                     
                                    


                                </div>
                            </div>
                        </div>
                    </div>   <!-- end row -->
                </aside><!-- end sidebar -->










                <!-- =========================================
                   Work experiences
                ==========================================-->

                <section class="col s12 m12 l8 section">
                    <div class="row">
                        <div class="full-portfolio">
                            <div class="wow fadeIn a1" data-wow-delay="0.1s">
                                <div class="portfolio-nav section-wrapper z-depth-1">
                                    <ul>
                                        <li class="filter" data-filter="all">Show All</li>
                                        <li class="filter" data-filter=".category-2">Websites</li>
                                        <li class="filter" data-filter=".category-3">Windows</li>
                                        <li class="filter" data-filter=".category-1">Android</li>
                                        <li class="filter" data-filter=".category-5">Extensions</li>
                                        <li class="filter" data-filter=".category-4">CISCO</li>
                                    </ul>
                                </div>
                                <div id="loader">
                                    <div class="loader-icon"></div>
                                </div>

                                <div class="screenshots" id="portfolio-item" >
                                    <div class="row">









                                    
                                        <ul class="grid">



                                            <!-- Portfolio one-->
                                            <li class="col m6 s1 2 mix category-1">
                                                <a href="javascript:void(0);" javascript="" class="sa-view-project-detail" data-action="#project-one">
                                                    <figure class="more">
                                                    <img src="../assets/images/jjacobo_images/ucontrolversion1.jpg" alt="ucontrolversion1" class="">
                                                        <figcaption>
                                                            <div class="caption-content">
                                                                <div class="single_image">
                                                                    <h2>UControl Phone</h2>
                                                                    <p>Schideron UControl Android App</p>
                                                                </div>							
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                			</li>


                                			<!-- Portfolio two-->
                                            <li class="col m6 s12 mix category-1">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail" data-action="#project-two">
                                                    <figure class="more">
                                                        <img src="../assets/images/jjacobo_images/uconverson2.png" alt="uconverson2" class="">
                                                        <figcaption>
                                                            <div class="caption-content">
                                                                <div class="single_image">
                                                                    <h2>UControl Tablet</h2>
                                                                    <p>Schideron UControl Android App</p>
                                                                </div>				
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>

                                            


                                            <!-- Portfolio three-->
                                            <li class="col m6 s12 mix category-1">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail" data-action="#project-three">
                                                    <figure class="more">
                                                    <img src="../assets/images/jjacobo_images/smarthome.png" alt="smarthome" class="">
                                                        <figcaption>
                                                            <div class="caption-content">
                                                                <div class="single_image">
                                                                    <h2>SmartHome Android</h2>
                                                                    <p>Perpetual Wave Smart App</p>
                                                                </div>								
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>


                                            <!-- Portfolio four-->
                                            <li class="col m6 s12 mix category-1">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail" data-action="#project-four">
                                                    <figure class="more">
                                                    <img src="../assets/images/jjacobo_images/medtronics.png" alt="medtronics" class="">
                                                        <figcaption>
                                                            <div class="caption-content">
                                                                <div class="single_image">
                                                                    <h2>Medtronic Android</h2>
                                                                    <p>Singapore Pharmacy Android System</p>
                                                                </div>	
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>


                                            <li class="col m6 s12 mix category-1">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail" data-action="#project-four">
                                                    <figure class="more">
                                                    <img src="../assets/images/jjacobo_images/foodstalk_app.png" alt="foodstalk app" class="">
                                                        <figcaption>
                                                            <div class="caption-content">
                                                                <div class="single_image">
                                                                    <h2>FoodStalk Android</h2>
                                                                    <p>Android app resturants reservation in subic bay.</p>
                                                                </div>	
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>


                                            <li class="col m6 s12 mix category-1">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail" data-action="#project-four">
                                                    <figure class="more">
                                                    <img src="../assets/images/jjacobo_images/foodstalk_app2.jpg" alt="foodstalk app" class="">
                                                        <figcaption>
                                                            <div class="caption-content">
                                                                <div class="single_image">
                                                                    <h2>FoodStalk Android</h2>
                                                                    <p>Android app resturants reservation in subic bay.</p>
                                                                </div>	
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>


                                            <li class="col m6 s12 mix category-1">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail" data-action="#project-four">
                                                    <figure class="more">
                                                    <img src="../assets/images/jjacobo_images/foodstalk_app3.jpg" alt="foodstalk app" class="">
                                                        <figcaption>
                                                            <div class="caption-content">
                                                                <div class="single_image">
                                                                    <h2>FoodStalk Android</h2>
                                                                    <p>Android app resturants reservation in subic bay.</p>
                                                                </div>	
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>

                                            <li class="col m6 s12 l6 mix category-2">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail" data-action="#project-five">
                                                    <figure class="more">
                                                    <img src="../assets/images/jjacobo_images/productcenter.PNG" alt="productcenter">
                                                        <figcaption>
                                                            <div class="caption-content">
                                                                <div class="single_image">
                                                                    <h2>Productcenter Webclient</h2>
                                                                    <p>Essig PLM System</p>
                                                                </div>									
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>


                                            <li class="col m6 s12 l6 mix category-2">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail" data-action="#project-five">
                                                    <figure class="more">
                                                    <img src="../assets/images/jjacobo_images/gladness_timesheet.png" alt="timesheet">
                                                        <figcaption>
                                                            <div class="caption-content">
                                                                <div class="single_image">
                                                                    <h2>Gladness Time Management System</h2>
                                                                    <p>Private agent use timesheet management system</p>
                                                                </div>									
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>


                                            <li class="col m6 s12 l6 mix category-2">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail" data-action="#project-five">
                                                    <figure class="more">
                                                    <img src="../assets/images/jjacobo_images/PORTFOLIO.JPG" alt="PORTFOLIO">
                                                        <figcaption>
                                                            <div class="caption-content">
                                                                <div class="single_image">
                                                                    <h2>My First Web Portfolio</h2>
                                                                    <p>My first use of BOOTSRAP API</p>
                                                                </div>									
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>



                                            <li class="col m6 s12 mix category-2">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail" data-action="#project-six">
                                                    <figure class="more">
                                                    <img src="../assets/images/jjacobo_images/puntabelle.png" alt="puntabelle"  class="">
                                                        <figcaption>
                                                            <div class="caption-content">
                                                                <div class="single_image">
                                                                    <h2>Puntabelle Website </h2>
                                                                    <p>Mabiga Bataan Puntabell Resort Reservation Website</p>
                                                                </div>					
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>




                                            <li class="col m6 s12 mix category-2">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail" data-action="#project-seven">
                                                    <figure class="more">
                                                    <img src="../assets/images/jjacobo_images/SOFTENG.JPG" alt="SOFTENG"  class="">
                                                        <figcaption>
                                                            <div class="caption-content">
                                                                <div class="single_image">
                                                                    <h2>St William Alumni Highschool</h2>
                                                                    <p>Alumni Website</p>
                                                                </div>					
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>




                                            <li class="col m6 s12 l6 mix category-2">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail"  data-action="#project-eight">
                                                    <figure class="more"> 
                                                    <img  src="../assets/images/jjacobo_images/samer.png" alt="samers">
                                                        <figcaption>
                                                            <div class="caption-content ">
                                                                <div class="single_image">
                                                                    <h2>Samers Website</h2>
                                                                    <p>Online Recruitment Website</p>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>






                                            <li class="col m6 s12 l6 mix category-3">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail"  data-action="#project-nine">
                                                    <figure class="more"> 
                                                    <img  src="../assets/images/jjacobo_images/CAPSTONE.JPG" alt="CAPSTONE.JPG">
                                                        <figcaption>
                                                            <div class="caption-content ">
                                                                <div class="single_image">
                                                                    <h2>G.H.N.H.S Library System </h2>
                                                                    <p>Windows based Library System</p>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>


                                            <li class="col m6 s12 l6 mix category-3">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail"  data-action="#project-ten">
                                                    <figure class="more"> 
                                                    <img  src="../assets/images/jjacobo_images/sbma.png" alt="sbma.png">
                                                        <figcaption>
                                                            <div class="caption-content ">
                                                                <div class="single_image">
                                                                    <h2>SBMA Job Order System</h2>
                                                                    <p>
                                                                  Windows based Job ordering.</p>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>

                                            <li class="col m6 s12 l6 mix category-3">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail"  data-action="#project-eleven">
                                                    <figure class="more"> 
                                                    <img  src="../assets/images/jjacobo_images/SAD.png" alt="SAD.png">
                                                        <figcaption>
                                                            <div class="caption-content ">
                                                                <div class="single_image">
                                                                    <h2>Paraon Dentist Management System</h2>
                                                                    <p>
                                                                Windows based dentist data management system</p>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>

             
                                            <li class="col m6 s12 l6 mix category-3">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail"  data-action="#project-tweleve">
                                                    <figure class="more"> 
                                                    <img  src="../assets/images/jjacobo_images/joborder.jpg" alt="joborder.jpg">
                                                        <figcaption>
                                                            <div class="caption-content ">
                                                                <div class="single_image">
                                                                    <h2>Subik Management System</h2>
                                                                    <p>
                                                                  Windows based telecommunication system </p>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>

                                    


                                            <li class="col m6 s12 l6 mix category-4">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail"  data-action="#project-thirtheen">
                                                    <figure class="more"> 
                                                    <img  src="../assets/images/jjacobo_images/NETWORK1.JPG" alt="NETWORK1.JPG">
                                                        <figcaption>
                                                            <div class="caption-content ">
                                                                <div class="single_image">
                                                                    <h2>Cisco Network Design</h2>
                                                                    <p>
                                                                    Designing network for private company. </p>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>


                                            <li class="col m6 s12 l6 mix category-4">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail"  data-action="#project-fourtheen">
                                                    <figure class="more"> 
                                                    <img  src="../assets/images/jjacobo_images/NETWORK2.JPG" alt="NETWORK2.JPG">
                                                        <figcaption>
                                                            <div class="caption-content ">
                                                                <div class="single_image">
                                                                <h2>Cisco Network Design</h2>
                                                                    <p>
                                                                    Designing network for private company. </p>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>

                                            <li class="col m6 s12 l6 mix category-4">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail"  data-action="#project-fiftheen">
                                                    <figure class="more"> 
                                                    <img  src="../assets/images/jjacobo_images/NETWORK3.JPG" alt="NETWORK3.JPG">
                                                        <figcaption>
                                                            <div class="caption-content ">
                                                                <div class="single_image">
                                                                <h2>Cisco Network Design</h2>
                                                                    <p>
                                                                    Designing network for private company. </p>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>

                                            <li class="col m6 s12 l6 mix category-4">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail"  data-action="#project-sixtheen">
                                                    <figure class="more"> 
                                                    <img  src="../assets/images/jjacobo_images/NETWORK4.JPG" alt="NETWORK4.JPG">
                                                        <figcaption>
                                                            <div class="caption-content ">
                                                                <div class="single_image">
                                                                <h2>Cisco Network Design</h2>
                                                                    <p>
                                                                    Designing network for private company. </p>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>

                                            <li class="col m6 s12 l6 mix category-4">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail"  data-action="#project-seventheen">
                                                    <figure class="more"> 
                                                    <img  src="../assets/images/jjacobo_images/NETWORK5.JPG" alt="NETWORK5.JPG">
                                                        <figcaption>
                                                            <div class="caption-content ">
                                                                <div class="single_image">
                                                                <h2>Cisco Network Design</h2>
                                                                    <p>
                                                                    Designing network for private company. </p>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>






                                            <li class="col m6 s12 l6 mix category-5">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail"  data-action="#project-eightheen">
                                                    <figure class="more"> 
                                                    <img  src="../assets/images/jjacobo_images/chrome_extension.png" alt="chrome_extension.png">
                                                        <figcaption>
                                                            <div class="caption-content ">
                                                                <div class="single_image">
                                                                    <h2>PCTR Chrome Extension</h2>
                                                                    <p>
                                                                 Browser Extension for PCTR Webclient </p>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>

                                            <li class="col m6 s12 l6 mix category-5">
                                                <a href="javascript:void(0);"  class="sa-view-project-detail"  data-action="#project-eightheen">
                                                    <figure class="more"> 
                                                    <img  src="../assets/images/jjacobo_images/firefox_extension.PNG" alt="firefox_extension.PNG">
                                                        <figcaption>
                                                            <div class="caption-content ">
                                                                <div class="single_image">
                                                                <h2>PCTR Firefox Extension</h2>
                                                                    <p>
                                                                 Browser Extension for PCTR Webclient </p>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </li>







                                        </ul>
                                  
                                  
                                  
                                    </div>
                                </div>


                                <!-- PROJECT DETAILS WILL BE LOADED HERE -->
                                <div class="sa-project-gallery-view" id="project-gallery-view"></div>
                                <div class="back-btn col s12">  
                                    <a id="back-button" class="btn btn-info waves-effect" href="#" ><i class="fa fa-long-arrow-left"></i> Go Back </a>
                                </div>



                            </div>                            
                        </div>












                        <!-- =======================================
                          portfolio Website
                        ==========================================-->
                        <div class="clear"></div>
                        <div class="section-wrapper z-depth-1 wow fadeIn" data-wow-delay="0.1s">                            
                            <div class="col s12 m12 l10 website right" >
                                <div class="row">
                                    <div class="col s12 m12 l6">
                                              <span><a href="http://www.jakejacobo.me/"  target="_blank">www.jakejacobo.me</a></span>
                                    </div>
                                    <div class="col col s12 m12 l6">
                                        <span><a href="https://ph.linkedin.com/in/jake-jacobo-5415b4b9" target="_blank">https://ph.linkedin.com/in/jake-jacobo-5415b4b9</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div><!-- end row -->
                </section>
                
                
                <!-- end section -->
            </div> <!-- end row -->
        </div>  <!-- end container -->
        
 <!--=====================
                JavaScript
        ===================== -->
        <!-- Jquery core js-->
        <script src="../assets/js/jquery.min.js"></script>
        
        <!-- materialize js-->
        <script src="../assets/js/materialize.min.js"></script>
        
        <!-- wow js-->
        <script src="../assets/js/wow.min.js"></script>
        
        <!-- Map api -->
        <script src="http://maps.googleapis.com/maps/api/js?v=3.exp"></script>
        
        <!-- Masonry js-->
        <script src="../assets/js/masonry.pkgd.js"></script>

        <script src="../assets/js/validator.min.js"></script>
        
        <script src="../assets/js/jquery.mixitup.min.js"></script>
        
        <!-- Customized js -->
        <script src="../assets/js/init.js"></script>
    

 
    </body>

</html>
