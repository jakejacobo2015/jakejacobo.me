<?php require_once '../jjacobo.php';?>
<!DOCTYPE html>
<!--[if IE 7]><html class="no-js ie7 oldie" lang="en-US"> <![endif]-->
<!--[if IE 8]><html class="no-js ie8 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">

<head>
        <meta charset="utf-8">
        
             
        <!-- TITLE OF SITE-->
        <title><?php echo $Full_Name; ?></title>
        
        <!-- META TAG -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="CV, Portfolio, Resume">
        <meta name="author" content="<?php echo $Full_Name; ?>">
        
        <!-- FAVICON -->
        <link rel="icon" href="../assets/images/JLOGO.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/apple-icon-76x76.html">
        <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/apple-icon-114x114.png">

        <!-- ========================================
                Stylesheets
        ==========================================--> 
        
        <!-- MATERIALIZE CORE CSS -->
        <link href="../assets/css/materialize.min.css" rel="stylesheet">
        

        <!-- ADDITIONAL CSS -->
        <link rel="stylesheet" href="../assets/css/animate.css">
        

        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,400italic,700italic' rel='stylesheet' type='text/css'>
        

        <!--FONTAWESOME CSS-->
        <link href="../assets/icons/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 
        

        <!-- CUSTOM STYLE -->
        <link href="../assets/css/nav.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">
        

        <!-- RESPONSIVE CSS-->
        <link href="../assets/css/responsive.css" rel="stylesheet">

        <!-- COLORS -->        
        <link rel="alternate stylesheet" href="../assets/css/colors/red.css" title="red">
        <link rel="alternate stylesheet" href="../assets/css/colors/purple.css" title="purple">
        <link rel="alternate stylesheet" href="../assets/css/colors/orange.css" title="orange">
        <link rel="alternate stylesheet" href="../assets/css/colors/green.css" title="green">
        <link rel="stylesheet" href="../assets/css/colors/lime.css" title="lime">
        <link rel="stylesheet" href="../assets/css/custom.css" title="lime">
        

        
    </head>
    <body>
        <div class="container">
            <div class="row">
                    <!-- Portfolio one-->
                    <div class="single-project col s12 m6 l12 z-depth-1" id="project-one">
                        <div class="row">
                            <div class="col s12 single-project-img">
                                <img src="assets/images/1.jpg" alt="" class="responsive-img">
                            </div>        
                                <div class="col s12 full-project">
                                    <h3 class="dark-text full-project-title">UControl Phone</h3>
                                
                                    <img class="img-responsive" src="../assets/images/jjacobo_images/ucontrolversion1.jpg" alt="image1" width="178" height="256"> 

                                </div>
                        </div>
                    </div>
           
              





                </div>
            </div>
        </div>
        
        <!--=====================
                JavaScript
        ===================== -->
        <!-- Jquery core js-->
        <script src="../assets/js/jquery.min.js"></script>
        
        <!-- materialize js-->
        <script src="../assets/js/materialize.min.js"></script>
        
        <!-- wow js-->
        <script src="../assets/js/wow.min.js"></script>
        
        <!-- Map api -->
        <script src="http://maps.googleapis.com/maps/api/js?v=3.exp"></script>
        
        <!-- Masonry js-->
        <script src="../assets/js/masonry.pkgd.js"></script>

        <script src="../assets/js/validator.min.js"></script>
        
        <script src="../assets/js/jquery.mixitup.min.js"></script>
        
        <!-- Customized js -->
        <script src="../assets/js/init.js"></script>
    
 
    </body>

</html>
