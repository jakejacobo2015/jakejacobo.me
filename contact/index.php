<?php require_once '../jjacobo.php';?>
<!DOCTYPE html>
<!--[if IE 7]><html class="no-js ie7 oldie" lang="en-US"> <![endif]-->
<!--[if IE 8]><html class="no-js ie8 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<head>
        <meta charset="utf-8">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140670496-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-140670496-1');
        </script>



        <!-- TITLE OF SITE-->
        <title><?php echo $Full_Name; ?></title>


        <!-- META TAG -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="CV, Portfolio, Resume">
        <meta name="author" content="<?php echo $Full_Name; ?>">
        
        <!-- FAVICON -->
        <link rel="icon" href="../assets/images/JLOGO.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/apple-icon-76x76.html">
        <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/apple-icon-114x114.png">

        <!-- ========================================
                Stylesheets
        ==========================================--> 
        
        <!-- MATERIALIZE CORE CSS -->
        <link href="../assets/css/materialize.min.css" rel="stylesheet">
        

        <!-- ADDITIONAL CSS -->
        <link rel="stylesheet" href="../assets/css/animate.css">
        

        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,400italic,700italic' rel='stylesheet' type='text/css'>
        

        <!--FONTAWESOME CSS-->
        <link href="../assets/icons/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 
        

        <!-- CUSTOM STYLE -->
        <link href="../assets/css/style.css" rel="stylesheet">
        

        <!-- RESPONSIVE CSS-->
        <link href="../assets/css/responsive.css" rel="stylesheet">
        
        
        <!-- COLORS -->        
        <link rel="alternate stylesheet" href="../assets/css/colors/red.css" title="red">
        <link rel="alternate stylesheet" href="../assets/css/colors/purple.css" title="purple">
        <link rel="alternate stylesheet" href="../assets/css/colors/orange.css" title="orange">
        <link rel="alternate stylesheet" href="../assets/css/colors/green.css" title="green">
        <link rel="stylesheet" href="../assets/css/colors/lime.css" title="lime"> 
        <link rel="stylesheet" href="../assets/css/custom.css">
        
        <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif] -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    </head>
    <body>
        <!-- Start Container-->
        <div class="container">
            <!-- row -->
            <div class="row">



     <!-- =========================================
                           SIDEBAR   
            ==========================================-->
                <!-- Start Sidebar -->
                <aside class="col l4 m12 s12 sidebar z-depth-1" id="sidebar">
                    <!--  Sidebar row -->
                    <div class="row">                      
                        <!--  top section   -->
                        <div class="heading">                            
                            <!-- ====================
                                      IMAGE   
                            ==========================-->
                            <div class="feature-img">
                                <a href="./"><img src="../assets/images/jakejacobo2x2.png" class="responsive-img" alt=""></a> 
                            </div>                            
                            <!-- =========================================
                                       NAVIGATION   
                            ==========================================-->
                            <div class=" nav-icon">
                                <nav>
                                    <div class="nav-wrapper">
                                      <ul id="nav-mobile" class="side-nav">                                  
                                        <li><a href="../">Home</a></li>          
                                        <li><a href="https://drive.google.com/file/d/1PWL0HU-dlyVWRpVqsPcmuj_AGWyfJiCX/view?usp=sharing"  target="_blank" >Resume</a></li>                                 
                                        <li><a href="../project">Projects</a></li>
                                        <li><a href="./">Contact</a></li>
                                      </ul>
                                      <a href="#" data-activates="nav-mobile" class="button-collapse  "><i class="mdi-navigation-menu"></i></a>
                                    </div>
                                </nav>
                            </div>                            
                            <!-- ========================================
                                       NAME AND TAGLINE
                            ==========================================--><br> 
                            <div class="title col s12 m12 l9 right  wow fadeIn" data-wow-delay="0.1s">
                                <h3 class="full_name"  style="font-size:37px !important;"><?php echo $Full_Name;?></h3> <!-- title name -->
                                <span class="work_title" style="font-size:15px;"><?php echo $Work_Title;  ?></span>  <!-- tagline -->
                            </div>                         
                        </div>
                         <!-- sidebar info -->
                        <div class="col l12 m12 s12 sort-info sidebar-item">
                            <div class="row">                               
                                <div class="col m12 s12 l3 icon"> <!-- icon -->
                                   <i class="fa fa-user"></i>
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a1" data-wow-delay="0.1s" > <!-- text -->
                                    <div class="section-item-details">
                                    <p><?php echo $Head_Details; ?></p>
                                    </div>             
                                </div>
                            </div>         
                        </div>
                        <!-- MOBILE NUMBER -->
                        <div class="col l12 m12 s12  mobile sidebar-item">
                            <div class="row">                                
                                <div class="col m12 s12 l3 icon">
                                    <i class="fa fa-phone"></i> <!-- icon -->
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a2" data-wow-delay="0.2s" >
                                    <div class="section-item-details">
                                        <div class="personal">
                                            <h4><a href="tel:<?php echo $Phone_Number; ?>"><?php echo $Phone_Number; ?></a></h4> <!-- Number -->             
                                            <span>mobile</span> 
                                        </div>
                                        <div class="work">
                                            <h4><a href="tel:<?php echo $Telephone_Number; ?>"><?php echo $Telephone_Number; ?></a></h4> <!-- Number -->
                                            <span>work</span> 
                                        </div>
                                    </div>
                                </div>
                            </div>             
                        </div>
                        <!--  EMAIL -->
                        <div class="col l12 m12 s12  email sidebar-item ">
                            <div class="row">                                
                                <div class="col m12 s12 l3 icon">
                                    <i class="fa fa-envelope"></i> <!-- icon -->
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a3" data-wow-delay="0.3s">
                                    <div class="section-item-details">
                                        <div class="personal">                                    
                                            <h4><a href="mailto:<?php echo $Email; ?>"><?php echo $Email; ?></a></h4> <!-- Email -->
                                            <span>personal</span> 
                                        </div>
                                        <div class="work">                                 
                                            <h4><a href="mailto:<?php echo $WorkEmail; ?>"><?php echo $WorkEmail; ?></a></h4> <!-- Email -->
                                            <span>work</span> <br>
                                        </div>
                                    </div>
                                </div> 
                            </div>          
                        </div>
                        <!-- ADDRESS  -->
                        <div class="col l12 m12 s12  address sidebar-item ">
                            <div class="row">                                
                                <div class="col l3 m12  s12 icon">
                                    <i class="fa fa-home"></i> <!-- icon -->
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a4" data-wow-delay="0.4s">
                                    <div class="section-item-details">
                                        <div class="address-details"> <!-- address  -->
                                            <h4><?php echo $Address; ?></span><br>
                                            <?php echo $District; ?>,  <?php echo $City; ?>.<br>
                                            <span><?php echo $State; ?>  <?php echo $ZipCode; ?></span></h4> 
                                        </div>                         
                                    </div>
                                </div>
                            </div>            
                        </div>
                        <!-- SKILLS -->
                        <div class="col l12 m12 s12  skills sidebar-item" >
                            <div class="row">
                                <div class="col m12 l3 s12 icon">
                                    <i class="fa fa-bar-chart-o"></i> <!-- icon -->
                                </div>
                                 <!-- Skills -->
                                <div class="col m12 l9 s12 skill-line a5 wow fadeIn" data-wow-delay="0.5s">
                                    <h3>Professional Skills </h3>
                                    
                            
                                    <span>HTML</span>
                                    <div class="progress">
                                        <div class="determinate"> 95%</div>
                                    </div>
                                    
                                    <span>CSS</span>
                                    <div class="progress">
                                        <div class="determinate">90%</div>
                                    </div>

                                    <span>Javascript</span>
                                    <div class="progress">
                                        <div class="determinate">85%</div>
                                    </div>

                                    <span>PHP</span>
                                    <div class="progress">
                                        <div class="determinate">70%</div>
                                    </div>

                                    <span>JAVA</span>
                                    <div class="progress">
                                        <div class="determinate">75%</div>
                                    </div>

                                     <span>XML</span>
                                    <div class="progress">
                                        <div class="determinate">45%</div>
                                    </div>

                                    <span>MYSQL</span>
                                    <div class="progress">
                                        <div class="determinate">40%</div>
                                    </div>

                                    <span>MONGODB</span>
                                    <div class="progress">
                                        <div class="determinate">40%</div>
                                    </div>


                                    <span>Node.js</span>                                    
                                    <div class="progress">
                                        <div class="determinate"> 70% </div>
                                    </div>
                                    
                                    <span>React-Native.js</span>                                    
                                    <div class="progress">
                                        <div class="determinate"> 40% </div>
                                    </div>

                                    <span>RubyOnRails</span>                                    
                                    <div class="progress">
                                        <div class="determinate"> 50% </div>
                                    </div>

                                     <span>C++</span>                                    
                                    <div class="progress">
                                        <div class="determinate"> 80% </div>
                                    </div>

                                    


                                </div>
                            </div>
                        </div>
                    </div>   <!-- end row -->
                </aside><!-- end sidebar -->




                <!-- =========================================
                         Google Map
                ===========================================-->

                 <section class="col s12 m12 l8 section">
                    <div class="row">
                        <!-- Start map -->
                        <div class="section-wrapper g-map z-depth-1">
                            <!-- <div id="map">  -->
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3691.033653732747!2d120.28254295022892!3d14.839078175115766!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x339670e2def75d93%3A0x288dcc2ec7025deb!2sOlongapo+City!5e1!3m2!1sen!2sph!4v1558370847255!5m2!1sen!2sph" width="100%" height="490" frameborder="0" style="border:0" allowfullscreen></iframe>
                            <!-- </div> -->
                      
                        </div> 
                        
                        <!--=======================================
                         Contact
                        ==========================================-->

                        <div class="section-wrapper z-depth-1">                            
                            <div class="section-icon col s12 m12 l2">
                                <i class="fa fa-paper-plane-o"></i>
                            </div>
                            <div class="col s12 m12 l10 wow fadeIn a1" data-wow-delay="0.1s">
                                
                                <h2>Contact</h2>                                
                                <div class="contact-form" id="contact">   
                                    <div class="row">                                   
                                        <form  id="contactForm" data-toggle="validator" action="sendMail.php" method="post" onsubmit="return isreCaptchaChecked()">
                                            <div id="msgSubmit" class="h3 text-center hidden"></div>
                                            <div class="input-field col s12">
                                                <label for="name" class="h4">Name *</label>
                                                <input type="text" class="form-control validate" id="name" name="name" required data-error="NEW ERROR MESSAGE">
                                            </div>
                                            <div class="input-field col s12">
                                                <label for="email" class="h4">Email *</label>
                                                <input type="email" class="form-control validate" id="email" name="email"  required>                
                                            </div>
                                            <div class="input-field col s12">
                                                <label for="message" class="h4 ">Message *</label>
                                                <textarea id="message" name="message" class="form-control materialize-textarea validate" required></textarea>           
                                            </div>


                            <div class="input-field col s12">
                                <div class="g-recaptcha" class="form-control validate"    data-sitekey="6LfHiKQUAAAAAGDff4z1HKbBe7yHMx3VrwjZJkqA" data-callback="recaptchaCallback"></div>
                              
           
                                <br>
                            </div>

                    

                                            <button type="submit" id="form-submit" class="btn btn-success">Submit</button>

                                        
                                            <div class="clearfix"></div>
                                            
                                                                            
                                        </form>     

                                                              
                                    </div> 
                                </div>
                            </div>                            
                         </div>
                        <!-- =========================================
                          portfolio Website
                        ==========================================-->

                        <div class="section-wrapper z-depth-1 wow fadeIn" data-wow-delay="0.1s">                            
                            <div class="col s12 m12 l10 website right" >
                                <div class="row">
                                    <div class="col s12 m12 l6">
                                    <span><a href="http://www.jakejacobo.me/"  target="_blank">www.jakejacobo.me</a></span>
                                    </div>
                                    <div class="col col s12 m12 l6">
                                        <span><a href="https://ph.linkedin.com/in/jake-jacobo-5415b4b9" target="_blank">https://ph.linkedin.com/in/jake-jacobo-5415b4b9</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                <!-- end row -->
                </section>
            </div> <!-- end row -->
        </div> <!-- end container -->
    
        <!--=====================
                JavaScript
        ===================== -->
        <!-- Jquery core js-->

<script>

var recaptchachecked=false; 
function recaptchaCallback() {
    recaptchachecked = true;
}


function isreCaptchaChecked()
{
    if(recaptchachecked == false){
        alert("Please Validate Captcha!")
    }
}
</script>
        
        <script src="../assets/js/jquery.min.js"></script>
        
        <!-- materialize js-->
        <script src="../assets/js/materialize.min.js"></script>
        
        <!-- wow js-->
        <script src="../assets/js/wow.min.js"></script>
        
        <!-- Map api -->
        <!-- <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=AIzaSyCRP2E3BhaVKYs7BvNytBNumU0MBmjhhxc"></script> -->
        
        <!-- Masonry js-->
        <script src="../assets/js/masonry.pkgd.js"></script>

        <script src="../assets/js/validator.min.js"></script>
        
        <script src="../assets/js/jquery.mixitup.min.js"></script>
        
        <!-- Customized js -->
        <script src="../assets/js/init.js"></script>
    

    </body>

</html>
